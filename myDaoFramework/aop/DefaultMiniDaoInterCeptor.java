package myDaoFramework.aop;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DefaultMiniDaoInterCeptor implements MiniDaoInterceptor {

	@Override
	public boolean onInsert(Field[] fields, Object obj) {
		Map<Object,Object> map = new HashMap<Object,Object>();
		for(int i=0;i<fields.length;i++){
			fields[i].setAccessible(true);
			String fieldName = fields[i].getName();
			if("createBy".equals(fieldName)){
				map.put("createBy","Sunny");
			}
			if("createDate".equals(fieldName)){
				map.put("createDate",new Date());
			}
			try {
				setFieldValue(map,obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}


	private void setFieldValue(Map<Object, Object> map, Object obj) throws Exception {
		Class clazz = obj.getClass();
		Method[] methods = clazz.getDeclaredMethods();
		Field[] fields = clazz.getDeclaredFields();
		for(Field f : fields){
			String fType = f.getType().getSimpleName();
			String fName = f.getName();
			String stterMethodName = parseSetterName(fName);
			if(!checkMethod(methods,stterMethodName))continue;
			if(!map.containsKey(fName))continue;
			Object value = map.get(fName);
			Method method = clazz.getMethod(stterMethodName,f.getType());
			if(null != value){
				if(String.class == f.getType()){
					method.invoke(obj, (String)value);
				}else if(Double.class == f.getType()){
					method.invoke(obj, (Double)value);
				}else if("int".equals(fType)){
					method.invoke(obj, Integer.valueOf((String)value));
				}else{
					method.invoke(obj, value);
				}
			}
		}
	}


	private boolean checkMethod(Method[] methods, String stterMethodName) {
		if(null != methods){
			for(Method m :methods){
				if(stterMethodName.equals(m.getName()))return true;
			}
		}
		return false;
	}


	private String parseSetterName(String fName) {
		if(null == fName || "".equals(fName)){
			return null;
		}
		return "set"+(char)((fName.charAt(0)-65)%32+65)+fName.substring(1)+fName.substring(1);
	}
	/*private String upperCasefirstLetter(String fName) {
		char[] chars = fName.toCharArray();
		
		String result = chars.toString();
		return result;
	}*/

	
	@Override
	public boolean onUpdate(Field[] fields, Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
