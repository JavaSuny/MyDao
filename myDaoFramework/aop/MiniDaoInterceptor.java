package myDaoFramework.aop;

import java.lang.reflect.Field;

public interface MiniDaoInterceptor {

	boolean onInsert(Field[] fields,Object obj);
	boolean onUpdate(Field[] fields,Object obj);
}
