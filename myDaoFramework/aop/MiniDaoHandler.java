package myDaoFramework.aop;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import myDaoFramework.annotations.Arguments;
import myDaoFramework.annotations.ResultType;
import myDaoFramework.util.FreemarkerParseFactory;
import myDaoFramework.util.MiniColumnMapRowMapper;
import myDaoFramework.util.MiniColumnOriginalMapRowMapper;
import myDaoFramework.util.MiniDaoConstants;
import myDaoFramework.util.MiniDaoPage;
import myDaoFramework.util.MiniDaoUtil;
import myDaoFramework.util.ParameterUtils;
import ognl.Ognl;
import ognl.OgnlException;

public class MiniDaoHandler implements InvocationHandler{

	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	private String UPPER_KEY = "upper";
	private String LOWER_KEY = "lower";
	private String keyType = "origin";
	private String dbType;
	private MiniDaoInterceptor miniDaoInterceptor; 
	@Override
	public Object invoke(Object proxy, Method method, Object[] objs) throws Throwable {
		Object returnObj = null;
		String templateSql = null;
		Map<String,Object> sqlParamsMap = new HashMap<String,Object>();
		MiniDaoPage pageSetting = new MiniDaoPage();
		installDaoMetaData(pageSetting,method,sqlParamsMap,objs);
		String executeSql = parseSqlTemplate(method,templateSql,sqlParamsMap);
		Map<String,Object> sqlMap = installPlaceholderSqlParam(executeSql,sqlParamsMap);
		returnObj = getReturnMiniDaiResult(dbType,pageSetting,method,executeSql,sqlMap);
		return returnObj;
	}
	private Object getReturnMiniDaiResult(String dbType2, MiniDaoPage pageSetting, Method method, String executeSql,
			Map<String, Object> sqlMap) {
		String methodName = method.getName();
		if(checkActiveKey(methodName) || checkActiveSql(executeSql)){
			if(sqlMap != null){
				return namedParameterJdbcTemplate.update(executeSql, sqlMap);
			}else{
				return jdbcTemplate.update(executeSql);
			}
		}else if(checkBatchKey(methodName)){
			return batchUpdate(executeSql);
		}else{
			Class<?> returnType = method.getReturnType();
			if(returnType.isPrimitive()){
				Number number = namedParameterJdbcTemplate.queryForObject(executeSql, sqlMap,BigDecimal.class);
				if("int".equals(returnType.getCanonicalName())){
					return number.intValue();
				}else if("long".equals(returnType.getCanonicalName())){
					return number.doubleValue();
				}else if("double".equals(returnType.getCanonicalName())){
					return number.doubleValue();
				}
			}else if(returnType.isAssignableFrom(List.class) || returnType.isAssignableFrom(MiniDaoPage.class)){
				int page = pageSetting.getPage();
				int rows = pageSetting.getRows();
				if(page!=0 && rows!=0){
					if(returnType.isAssignableFrom(MiniDaoPage.class)){
						if(sqlMap != null){
							pageSetting.setTotal(namedParameterJdbcTemplate.queryForObject(getCountSql(executeSql), sqlMap, Integer.class));
						}else{
							pageSetting.setTotal(jdbcTemplate.queryForObject(getCountSql(executeSql), Integer.class));
						}
					}
					executeSql = MiniDaoUtil.createPageSql(dbType,executeSql,page,rows);
				}
				RowMapper resultType = getListRealType(method);
				List list;
				if(sqlMap != null){
					list = namedParameterJdbcTemplate.query(executeSql, sqlMap,resultType);
				}else{
					list = jdbcTemplate.query(executeSql,resultType);
				}
				if(returnType.isAssignableFrom(MiniDaoPage.class)){
					pageSetting.setResults(list);
					return pageSetting;
				}else{
					return list;
				}
			}else if(returnType.isAssignableFrom(Map.class)){
				if(sqlMap != null){
					return namedParameterJdbcTemplate.queryForObject(executeSql, sqlMap, getColumnMapRowMapper());
				}else{
					return jdbcTemplate.queryForObject(executeSql, getColumnMapRowMapper());
				}
			} else if (returnType.isAssignableFrom(String.class)) {
				if (sqlMap != null) {
					return namedParameterJdbcTemplate.queryForObject(executeSql, sqlMap, String.class);
				} else {
					return jdbcTemplate.queryForObject(executeSql, String.class);
				}
			} else if (MiniDaoUtil.isWrapClass(returnType)) {
				if (sqlMap != null) {
					return namedParameterJdbcTemplate.queryForObject(executeSql, sqlMap, returnType);
				} else {
					return jdbcTemplate.queryForObject(executeSql, returnType);
				}
			}else{
				RowMapper<?> rm = BeanPropertyRowMapper.newInstance(returnType);
				if (sqlMap != null) {
					return namedParameterJdbcTemplate.queryForObject(executeSql, sqlMap, rm);
				} else {
					return jdbcTemplate.queryForObject(executeSql, rm);
				}
			}
		}
		return null;
	}
	private RowMapper<?> getListRealType(Method method) {
		ResultType resultType = method.getAnnotation(ResultType.class);
		if(resultType != null){
			if(resultType.value().equals(Map.class)){
				return getColumnMapRowMapper();
			}
			return BeanPropertyRowMapper.newInstance(resultType.value());
		}
		String genericReturnType = method.getGenericReturnType().toString();
		String realType = genericReturnType.replace("java.util.list","").replace("<","").replace(">","");
		if(realType.contains("java.util.map")){
			return getColumnMapRowMapper();
		}else if(realType.length() > 0){
			try {
				return BeanPropertyRowMapper.newInstance(Class.forName(realType));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException("Class for name : " + realType + "whit a erorr!");
			}
		}
		return getColumnMapRowMapper();
	}
	private RowMapper<Map<String,Object>> getColumnMapRowMapper() {
		if(getKeyType().equalsIgnoreCase(LOWER_KEY)){
			return new MiniColumnMapRowMapper();
		}else if(getKeyType().equalsIgnoreCase(UPPER_KEY)){
			return new ColumnMapRowMapper();
		}else {
			return new MiniColumnOriginalMapRowMapper();
		}
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	
	public MiniDaoInterceptor getEmptyInterceptor() {
		return miniDaoInterceptor;
	}

	public void setMiniDaoInterceptor(MiniDaoInterceptor miniDaoInterceptor) {
		this.miniDaoInterceptor = miniDaoInterceptor;
	}
	public String getDbType() {
		return dbType;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public String getKeyType() {
		return keyType;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}
	private String getCountSql(String executeSql) {
		executeSql = removeOrderBy(executeSql);
		return "select count(0) from ("+executeSql+") temp_count";
	}
	public String removeOrderBy(String sql) {
		if(sql==null){
			return null;
		}
		sql = sql.replaceAll("(?i)order by [\\s|\\S]+$", "");  
	   return sql;  
	} 
	private Object batchUpdate(String executeSql) {
		String[] sqls = executeSql.split(",");
		if(sqls.length < 100){
			return jdbcTemplate.batchUpdate(sqls);
		}
		int[] result = new int[sqls.length];
		List<String> sqlList = new ArrayList<String>();
		for(int i=0 ; i<sqls.length ; i++){
			sqlList.add(sqls[i]);
			if(i%100 == 0){
				addResultArray(result,i+1,jdbcTemplate.batchUpdate(sqlList.toArray(new String[0])));
				sqlList.clear();
			}
		}
		addResultArray(result,sqls.length,jdbcTemplate.batchUpdate(sqlList.toArray(new String[0])));
		return result;
	}
	private void addResultArray(int[] result, int i, int[] batchUpdate) {
		int length = batchUpdate.length;
		for(int j=0 ; j<length ; j++){
			result[i-length+j] = batchUpdate[i];
		}
	}
	private boolean checkBatchKey(String methodName) {
		String[] keys = MiniDaoConstants.INF_METHOD_BATCH.split(",");
		for(String s : keys){
			if(methodName.startsWith(s)){
				return true;
			}
		}
		return false;
	}
	private boolean checkActiveSql(String executeSql) {
		executeSql = executeSql.trim().toLowerCase();
		String[] keys = MiniDaoConstants.INF_METHOD_ACTIVE.split(",");
		for(String s : keys){
			if(executeSql.startsWith(s)){
				return true;
			}
		}
		return false;
	}
	private boolean checkActiveKey(String methodName) {
		String[] keys = MiniDaoConstants.INF_METHOD_ACTIVE.split(",");
		for(String s : keys){
			if(methodName.startsWith(s)){
				return true;
			}
		}
		return false;
	}
	private Map<String, Object> installPlaceholderSqlParam(String executeSql, Map<String, Object> sqlParamsMap) throws OgnlException {
		Map<String,Object> map = new HashMap<String,Object>();
		String regex = ":[ tnx0Bfr]*[0-9a-z.A-Z_]+";
		Pattern pat = Pattern.compile(regex);
		Matcher m = pat.matcher(executeSql);
		while(m.find()){
			String ognlKey = m.group().replace(":","").trim();
			map.put(ognlKey, Ognl.getValue(ognlKey,sqlParamsMap));
		}
		return map;
	}
	private String parseSqlTemplate(Method method, String templateSql, Map<String, Object> sqlParamsMap) {
		String executeSql = null;
		String sqlTemplatePath = method.getDeclaringClass().getName().replace('.', '/').replace("/dao/","/sql/")+"_"+method.getName()+".sql";
		if(!FreemarkerParseFactory.isExistTemplate(sqlTemplatePath)){
			sqlTemplatePath = method.getDeclaringClass().getName().replace(".", "/") + "_" + method.getName() + ".sql";
		}
		executeSql = FreemarkerParseFactory.parseTemplate(sqlTemplatePath, sqlParamsMap);
		return executeSql;
	}
	private void installDaoMetaData(MiniDaoPage pageSetting, Method method, Map<String, Object> sqlParamsMap,
			Object[] objs) throws Exception {
		if(miniDaoInterceptor != null && objs != null && objs.length == 1){
			String methodName = method.getName();
			Object obj = objs[0];
			Field[] fields = obj.getClass().getDeclaredFields();
			if(methodName.startsWith("insert")){
				miniDaoInterceptor.onInsert(fields, obj);
			}
			if(methodName.startsWith("update")){
				miniDaoInterceptor.onUpdate(fields, obj);
			}
			String templateSql = null;
			boolean withAnnotationArgs = method.isAnnotationPresent(Arguments.class);
			if(withAnnotationArgs){
				Arguments args = method.getAnnotation(Arguments.class);
				if(args.value().length != objs.length){
					throw new Exception("参数数量不一致！");
				}
				int objNum = 0;
				for(String v : args.value()){
					if(v.equalsIgnoreCase("page")){
						pageSetting.setPage(Integer.valueOf(objs[objNum].toString()));
					}
					if(v.equalsIgnoreCase("rows")){
						pageSetting.setRows(Integer.parseInt(objs[objNum].toString()));
					}
					sqlParamsMap.put(v, objs[objNum]);
					objNum++;
				}
			}else{
				if(objs != null && objs.length >=1){
					String[] params = ParameterUtils.getMethodParameterNamesByAnnotation(method);
					if(params == null || params.length == 0){
						throw new Exception("多于一个参数，必须使用： 方法标签@Arguments或 参数标签@Parameter");
					}
					if(params.length != objs.length){
						throw new Exception("多于一个参数，必须使用：  参数标签@Parameter");
					}
					int objNum = 0;
					for(String v : params){
						if(v == null){
							throw new Exception("Dao接口定义，所有参数必须使用@Parameter标签");
						}
						if(v.equalsIgnoreCase("page")){
							pageSetting.setPage(Integer.valueOf(objs[objNum].toString()));
						}
						if(v.equalsIgnoreCase("rows")){
							pageSetting.setRows(Integer.parseInt(objs[objNum].toString()));
						}
						sqlParamsMap.put(v, objs[objNum]);
						objNum++;
					}
				}else if(objs != null && objs.length == 1){
					sqlParamsMap.put(MiniDaoConstants.SQL_FTL_DTO,objs[0]);
				}
			}
		}
	}

}
