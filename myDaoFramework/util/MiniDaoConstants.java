package myDaoFramework.util;

public class MiniDaoConstants {

	public static final String INF_METHOD_ACTIVE = "insert,add,create,update,modify,store,delete,remove";
	public static final String INF_METHOD_BATCH = "batch";
	public static final String SQL_FTL_DTO = "dto";
}
