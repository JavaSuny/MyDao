package myDaoFramework.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import myDaoFramework.annotations.Parameter;

public class ParameterUtils {

	public static String[] getMethodParameterNamesByAnnotation(Method method){
		Annotation[][] parameterAnnotations = method.getParameterAnnotations();
		if(parameterAnnotations == null || parameterAnnotations.length == 0){
			return null;
		}
		String[] parameterNames = new String[parameterAnnotations.length];
		int i = 0;
		for(Annotation[] parameterAnnotation : parameterAnnotations){
			for(Annotation annotation : parameterAnnotation){
				if(annotation instanceof Parameter){
					Parameter parameter = (Parameter)annotation;
					parameterNames[i++] = parameter.value();
				}
			}
		}
		return parameterNames;
	}
}
