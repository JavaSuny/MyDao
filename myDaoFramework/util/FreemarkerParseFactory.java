package myDaoFramework.util;

import java.io.StringWriter;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreemarkerParseFactory {

	private static final String ENCODE = "UTF-8";
	private static final Configuration TPLCONF = new Configuration();
	private static final Configuration SQLCONF = new Configuration();
	private static StringTemplateLoader STL = new StringTemplateLoader();
	 private final static Pattern p = Pattern
	            .compile("(?ms)/\\*.*?\\*/|^\\s*//.*?$");

	    static {
	    	TPLCONF.setClassForTemplateLoading(
	                new FreemarkerParseFactory().getClass(), "/");
	    	TPLCONF.setNumberFormat("0.#####################");
	    	SQLCONF.setTemplateLoader(STL);
	    	SQLCONF.setNumberFormat("0.#####################");
	    }
	    public static boolean isExistTemplate(String tplName) {
	        try {
	            Template mytpl = TPLCONF.getTemplate(tplName,ENCODE);
	            if (mytpl == null) {
	                return false;
	            }
	        } catch (Exception e) {
	            return false;
	        }
	        return true;
	    }
	    public static String parseTemplate(String tplName, Map<String, Object> paras) {
	        try {
	            StringWriter swriter = new StringWriter();
	            Template mytpl = TPLCONF.getTemplate(tplName, ENCODE);
	            mytpl.process(paras, swriter);
	            return getSqlText(swriter.toString());
	        } catch (Exception e) {
	        	e.printStackTrace();
	            throw new RuntimeException("解析SQL模板异常");
	        }
	    }
	    public static String parseTemplateContent(String tplContent,
	                                              Map<String, Object> paras) {
	        try {
	            StringWriter swriter = new StringWriter();
	            if (STL.findTemplateSource("sql_" + tplContent.hashCode()) == null) {
	                STL.putTemplate("sql_" + tplContent.hashCode(), tplContent);
	            }
	            Template mytpl = SQLCONF.getTemplate("sql_" + tplContent.hashCode(), ENCODE);
	            mytpl.process(paras, swriter);
	            return getSqlText(swriter.toString());
	        } catch (Exception e) {
	        	e.printStackTrace();
	            throw new RuntimeException("解析SQL模板异常");
	        }
	    }
	    private static String getSqlText(String sql) {
	        sql = p.matcher(sql).replaceAll("");
	        sql = sql.replaceAll("\\n", " ").replaceAll("\\t", " ")
	                .replaceAll("\\s{1,}", " ").trim();
	        if (sql.endsWith("where") || sql.endsWith("where ")) {
	            sql = sql.substring(0, sql.lastIndexOf("where"));
	        }
	        int index = 0;
	        while ((index = StringUtils.indexOfIgnoreCase(sql, "where and", index)) != -1) {
	            sql = sql.substring(0, index + 5)
	                    + sql.substring(index + 9, sql.length());
	        }
	        index = 0;
	        while ((index = StringUtils.indexOfIgnoreCase(sql, ", where", index)) != -1) {
	            sql = sql.substring(0, index)
	                    + sql.substring(index + 1, sql.length());
	        }
	        if (sql.endsWith(",") || sql.endsWith(", ")) {
	            sql = sql.substring(0, sql.lastIndexOf(","));
	        }
	        return sql;
	    }
}
