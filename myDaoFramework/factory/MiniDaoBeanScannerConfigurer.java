package myDaoFramework.factory;

import java.lang.annotation.Annotation;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import myDaoFramework.aop.MiniDaoHandler;
import myDaoFramework.aop.MiniDaoInterceptor;

public class MiniDaoBeanScannerConfigurer implements BeanDefinitionRegistryPostProcessor {

	private String basePackage;
	private Class<? extends Annotation> annontation = Repository.class;
	private String keyType = "origin";
	private String dbType;
	private MiniDaoInterceptor miniDaoInterceptor;
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory registry) throws BeansException {
		registry = (BeanDefinitionRegistry)registry;
		registerRequestProxyHandler(registry);
		ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry,annotation);
		scanner.scan(StringUtils.tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS));
	}
	private void registerRequestProxyHandler(ConfigurableListableBeanFactory registry) {
			GenericBeanDefinition jdbcDaoProxyDefinition = new GenericBeanDefinition();
			jdbcDaoProxyDefinition.setBeanClass(MiniDaoHandler.class);
			jdbcDaoProxyDefinition.getPropertyValues().add("keyType", keyType);
			jdbcDaoProxyDefinition.getPropertyValues().add("dbType", dbType);
			jdbcDaoProxyDefinition.getPropertyValues().add("miniDaoInterceptor", miniDaoInterceptor);
			registry.registerBeanDefinition("miniDaoHandler", jdbcDaoProxyDefinition);
	}
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry arg0) throws BeansException {
		
	}
	public String getBasePackage() {
		return basePackage;
	}
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}
	public Class<? extends Annotation> getAnnontation() {
		return annontation;
	}
	public void setAnnontation(Class<? extends Annotation> annontation) {
		this.annontation = annontation;
	}
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public MiniDaoInterceptor getMiniDaoInterceptor() {
		return miniDaoInterceptor;
	}
	public void setMiniDaoInterceptor(MiniDaoInterceptor miniDaoInterceptor) {
		this.miniDaoInterceptor = miniDaoInterceptor;
	}
	
}
